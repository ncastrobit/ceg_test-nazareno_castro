export class Library {
	constructor(
		public name: string,
		public book_count: number,
		public author_count: number,
	){}
}

export class LibrariesPage {
	constructor(
		public count: number,
	    public next: string,
	    public previous: string,
	    public results: Library[],
	){}
}