from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.core import management
import datetime
from book.models import Author, Book, Library
from rest_framework.test import APIClient
from django.urls import reverse
import json


BOOKS_URL = reverse('book-list')
AUTHOR_URL = reverse('author-list')
LIBRARY_URL = reverse('library-list')

def create_two_books():
    author = Author.objects.create(first_name='Django', last_name='Test')
    books = [
        Book.objects.create(author=author, title='Zen'),
        Book.objects.create(author=author, title='Awesome')
    ]
    return books

def create_eleven_books():
    author = Author.objects.create(first_name='Django', last_name='Test')
    books = [
        Book.objects.create(author=author, title='Zen'),
        Book.objects.create(author=author, title='Awesome'),
        Book.objects.create(author=author, title='Cool'),
        Book.objects.create(author=author, title='Bad'),
        Book.objects.create(author=author, title='Worst'),
        Book.objects.create(author=author, title='Average'),
        Book.objects.create(author=author, title='Ordinary'),
        Book.objects.create(author=author, title='Normal'),
        Book.objects.create(author=author, title='Horrible'),
        Book.objects.create(author=author, title='Wonderfull'),
        Book.objects.create(author=author, title='Better')
    ]
    return books

def create_thirty_one_authors():
    authors = []
    for each in range(0, 31):
        new_author = Author.objects.create(first_name='Django', last_name='Test')
        authors.append(new_author)
    return authors

def authenticated_client():
    user = User.objects.create_user(username='john',
                                 email='jlennon@beatles.com',
                                 password='glass onion')
    client = APIClient()
    token = Token.objects.get(user__username='john')
    client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
    return client



def create_eleven_books_two_autors_one_library():
    library = Library.objects.create(name="Alexandria")
    author_one = Author.objects.create(first_name='Django', last_name='Test')
    author_two = Author.objects.create(first_name='Django-Rest', last_name='Framework')
    books = []
    for each in range(0, 5):
        new_book = Book.objects.create(author=author_one, title='Some Title')
        new_book.libraries.add(library)
        books.append(new_book)
    for each in range(0, 6):
        new_book = Book.objects.create(author=author_two, title='Some Title')
        books.append(new_book)
        new_book.libraries.add(library)
    return books
    

def create_thirty_one_libraries():
    for each in range(0, 31):
        Library.objects.create(name="Alexandria")



class BooksTestCase(TestCase):

    def test_when_get_books_return_name_author_and_libraries_list(self):
        author = Author.objects.create(first_name='Django', last_name='Test')
        Book.objects.create(author=author, title='Zen')
        client = authenticated_client()
        response = client.get(BOOKS_URL)
        json_books = response.json()
        book = json_books.get('results')[0]
        self.assertEquals('Zen', book.get('title'))
        self.assertEquals('Django', book.get('author').get('first_name'))
        self.assertEquals('Test', book.get('author').get('last_name'))

    def test_when_get_books_return_list_ordered_by_title_from_a_to_z(self):
        create_two_books()
        client = authenticated_client()
        response = client.get(BOOKS_URL)
        json_books = response.json()
        first_book = json_books.get('results')[0]
        second_book = json_books.get('results')[1]
        self.assertLessEqual(first_book.get('title'), second_book.get('title'))

    def test_when_get_books_return_ten_results(self):
        create_eleven_books()
        client = authenticated_client()
        response = client.get(BOOKS_URL)
        json_books = response.json().get('results')
        self.assertEquals(10, len(json_books))

    def test_books_view_only_accesible_by_authenticated_user(self):
        client = APIClient()
        response = client.get(BOOKS_URL)
        self.assertEquals(401, response.status_code)
        auth_client = authenticated_client()
        response = auth_client.get(BOOKS_URL)
        self.assertEquals(200, response.status_code)


class AuthorTestCase(TestCase):

    def test_when_get_libraries_return_name_with_authors_and_books_amount(self):
        create_eleven_books()
        client = authenticated_client()
        response = client.get(AUTHOR_URL)
        json_authors = response.json()
        author = json_authors.get('results')[0]
        self.assertEquals(11, author.get('book_count'))


    def test_when_get_libraries_return_30_results(self):
        create_thirty_one_authors()
        client = authenticated_client()
        response = client.get(AUTHOR_URL)
        json_authors = response.json()
        authors = json_authors.get('results')
        self.assertEquals(10, len(authors))


    def test_author_view_only_accesible_by_authenticated_user(self):
        client = APIClient()
        response = client.get(AUTHOR_URL)
        self.assertEquals(401, response.status_code)
        auth_client = authenticated_client()
        response = auth_client.get(AUTHOR_URL)
        self.assertEquals(200, response.status_code)




class LibraryAuthorTestCase(TestCase):

    def test_when_get_library_return_name_with_amount_of_books_and_amount_of_authors(self):
        create_eleven_books_two_autors_one_library()
        client = authenticated_client()
        response = client.get(LIBRARY_URL)
        json_library = response.json()
        library = json_library.get('results')[0]
        self.assertEquals(11, library.get('book_count'))
        self.assertEquals(2, library.get('author_count'))
        self.assertEquals(1, len(json_library.get('results')))

    def test_when_get_library_return_30_results(self):
        create_thirty_one_libraries()
        client = authenticated_client()
        response = client.get(LIBRARY_URL)
        json_library = response.json()
        self.assertEquals(10, len(json_library.get('results')))


    def test_when_are_to_many_authors_the_response_take_less_than_one_second(self):
        management.call_command('loaddata', 'data.json', verbosity=0)
        
        client = authenticated_client()
        t0 = datetime.datetime.now()
        response = client.get(LIBRARY_URL)
        t1 = datetime.datetime.now()
        result = t1 - t0
        self.assertLessEqual(0, result.seconds)

    def test_author_view_only_accesible_by_authenticated_user(self):
        client = APIClient()
        response = client.get(LIBRARY_URL)
        self.assertEquals(401, response.status_code)
        auth_client = authenticated_client()
        response = auth_client.get(LIBRARY_URL)
        self.assertEquals(200, response.status_code)
