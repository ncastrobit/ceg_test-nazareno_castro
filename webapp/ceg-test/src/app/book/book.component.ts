import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../services/general.service'
import { BooksPage } from '../model/book'

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})

export class BookComponent implements OnInit {

  books: BooksPage;

  constructor(private service: GeneralService) {}

  ngOnInit() {
  	this.service.getBooks().subscribe(
      data => {
      	console.log(data);
       this.books = data;
      },
      error => console.log(error)
    );

  }

  changePage(pageUrl: string){
    this.service.getPage(pageUrl).subscribe(
      data => {
        console.log(data);
       this.books = data;
      },
      error => console.log(error)
    );
  }

}
