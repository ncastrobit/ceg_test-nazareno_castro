import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../services/general.service'
import { LibrariesPage } from '../model/library'

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})

export class LibraryComponent implements OnInit {

  libraries: LibrariesPage;

  constructor(private service: GeneralService) {}

  ngOnInit() {
  	this.service.getLibraries().subscribe(
      data => {
      	console.log(data);
       this.libraries = data;
      },
      error => console.log(error)
    );

  }

  changePage(pageUrl: string){
    this.service.getPage(pageUrl).subscribe(
      data => {
        console.log(data);
       this.libraries = data;
      },
      error => console.log(error)
    );
  }

}