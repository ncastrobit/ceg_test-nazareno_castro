import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http'; import { HttpModule } from '@angular/http';

@Injectable()
export class AuthguardGuard implements CanActivate {

   constructor(private router: Router) { }

  canActivate(){
    if (localStorage.getItem('supertoken')) {
        // logged in so return true
        return true;
    }else {
		  this.router.navigate(['/login']);
		  return false;
    }
  }
}
