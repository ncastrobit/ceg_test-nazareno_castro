import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../services/authservice.service';
import { Router } from '@angular/router';
import { User } from '../model/user'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ User ]
})


export class LoginComponent implements OnInit {


  public errorMessage: String = "";
  public isVisible: boolean = false;

  constructor(
  	private authenticationService: AuthserviceService,
  	private router: Router,
  	private user: User
  ) { }
  ngOnInit() {
  }

  login() {
    this.authenticationService.login(this.user.username, this.user.password)
	    .subscribe(
	        data => {
	        	localStorage.setItem('supertoken', data['token']);
				    this.router.navigate(['/home'])
	        },
	        error => {
	           this.errorMessage = "User or Password Invalid";
             this.isVisible = true;
        	});
    }
}

