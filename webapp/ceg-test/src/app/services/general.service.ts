import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GeneralService {

  constructor(public http: HttpClient) { }

  public getBooks(): Observable<any> {
    return this.http.get<any>('http://localhost:8000/books/')
  }

  public getLibraries(): Observable<any> {
    return this.http.get<any>('http://localhost:8000/libraries/')
  }

  public getAuthors(): Observable<any> {
    return this.http.get<any>('http://localhost:8000/authors/')
  }

  public getPage(pageUrl: string): Observable<any> {
    return this.http.get<any>(pageUrl)
  }
}
