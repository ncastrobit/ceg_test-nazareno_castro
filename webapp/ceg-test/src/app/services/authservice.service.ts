import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class AuthserviceService {

  constructor(public http: HttpClient, public router: Router) { }

  public getToken(): string {
    return localStorage.getItem('supertoken');
  }

   public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return token != undefined;
  }

  public login(username: string, password: string): Observable<any> {
    return this.http.post('http://localhost:8000/api-token-auth/', {'username': username, 'password': password}, httpOptions)
  }

  public logOut(){
    localStorage.removeItem('supertoken');
    this.router.navigate(['/login']);
  }


}
