import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../services/general.service';
import { AuthorPage } from '../model/author';


@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})

export class AuthorComponent implements OnInit {

  authors: AuthorPage;

  constructor(private service: GeneralService) {}

  ngOnInit() {
  	this.service.getAuthors().subscribe(
      data => {
      	console.log(data);
       this.authors = data;
      },
      error => console.log(error)
    );

  }

  changePage(pageUrl: string){
    this.service.getPage(pageUrl).subscribe(
      data => {
       this.authors = data;
      },
      error => console.log(error)
    );
  }

}


