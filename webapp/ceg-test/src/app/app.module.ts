import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AuthguardGuard} from './guards/authguard.guard'
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { TokenInterceptor } from './interceptor/token.interceptor';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { BookComponent } from './book/book.component';
import { LibraryComponent } from './library/library.component'
import { AuthorComponent } from './author/author.component';

import { GeneralService } from './services/general.service';
import {AuthserviceService} from './services/authservice.service';
import { LogoutComponent } from './logout/logout.component';

const appRoutes: Routes = [
  { path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    canActivate: [AuthguardGuard]
  },
  { path: 'home',
    component: HomeComponent,
    canActivate: [AuthguardGuard]
  },
  { path: 'login',
    component: LoginComponent,
  },
  {
    path: 'books',
    component: BookComponent,
     canActivate: [AuthguardGuard]
  },
  {
    path: 'authors',
    component: AuthorComponent,
     canActivate: [AuthguardGuard]
  }, 
  {
  	path: 'libraries',
  	component: LibraryComponent,
     canActivate: [AuthguardGuard]
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthguardGuard]
  },
  {path: '**', redirectTo: '/home'}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    BookComponent,
    AuthorComponent,
    LibraryComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
  	AuthguardGuard, 
  	AuthserviceService,
  	GeneralService,
  	{
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
