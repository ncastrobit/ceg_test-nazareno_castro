from book.serializer import BookSerializer, AuthorSerializer, LibrarySerializer
from book.models import Book, Author, Library
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

class TenResultsPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.order_by('title')
    serializer_class = BookSerializer
    pagination_class = TenResultsPagination
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    pagination_class = TenResultsPagination
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

class LibraryViewSet(viewsets.ModelViewSet):
    queryset = Library.objects.all()
    serializer_class = LibrarySerializer
    pagination_class = TenResultsPagination
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
