export class Author {
	constructor(
		public first_name: string,
		public last_name: string,
		public book_count: number,
	){}
}

export class AuthorPage {
	constructor(
		public count: number,
	    public next: string,
	    public previous: string,
	    public results: Author[],
	){}
}