
from rest_framework import serializers
from book.models import Book, Author, Library

class BookLibrarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Library
        fields = ('name',)


class BookAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('first_name', 'last_name',)


class BookSerializer(serializers.ModelSerializer):
    author = BookAuthorSerializer()
    libraries = BookLibrarySerializer(read_only=True, many=True)

    class Meta:
        model = Book
        fields = ('title','author', 'libraries')


class AuthorSerializer(serializers.ModelSerializer):
    book_count = serializers.SerializerMethodField()
    
    class Meta:
        model = Author
        fields = ('first_name', 'last_name', 'book_count')

    def get_book_count(self, author):
        return Book.objects.filter(author=author).count()


class LibrarySerializer(serializers.ModelSerializer):
    book_count = serializers.SerializerMethodField()
    author_count = serializers.SerializerMethodField()
    class Meta:
        model = Library
        fields = ('name', 'book_count', 'author_count')

    def get_book_count(self, library):
        return library.book_set.count()

    def get_author_count(self, library):
        authors_count = Author.objects.filter(book__libraries__pk=library.pk).distinct().count()
        return authors_count