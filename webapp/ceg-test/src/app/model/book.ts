export class Book {
	constructor(
		public title: string,
		public author: AuthorBook,
		public libraries: LibraryBook[],
	){}
}


export class AuthorBook {
	constructor(
		public first_name: string,
		public last_name: string,
	){}	
}


export class LibraryBook {
	constructor(
		public name: string,
	){}	
}

export class BooksPage {
	constructor(
		public count: number,
	    public next: string,
	    public previous: string,
	    public results: Book[],
	){}
}